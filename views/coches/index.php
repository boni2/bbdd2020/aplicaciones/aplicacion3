<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coches-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Coches', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cod_cliente',
            'marca',
            'fecha',
            'precio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
