<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coches".
 *
 * @property int $id
 * @property string|null $cod_cliente
 * @property string|null $marca
 * @property string|null $fecha
 * @property float|null $precio
 *
 * @property Clientes $clientes
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['fecha'], 'safe'],
            [['precio'], 'number'],
            [['cod_cliente', 'marca'], 'string', 'max' => 20],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_cliente' => 'Cod Cliente',
            'marca' => 'Marca',
            'fecha' => 'Fecha',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[Clientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasOne(Clientes::className(), ['id_coche_alquilado' => 'id']);
    }
}
